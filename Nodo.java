package SI_TP2;

import java.util.ArrayList;

public class Nodo {
  int estado, valor, profundidad;
  ArrayList<Nodo> hijos = new ArrayList<>();

  private static int funcionEvaluacion(int estado) {
    return estado * estado;
  }

  public Nodo obtenerVecino(boolean maximizar) {
    int n = obtenerHijos();
    Nodo vecinoMasAlto = hijos.get(0);
    for (int i = 1; i < n; i++) {
      Nodo vecino = hijos.get(i);
      if (maximizar) {
        if (vecino.valor > vecinoMasAlto.valor) {
          vecinoMasAlto = vecino;
        }
      } else {
        if (vecino.valor < vecinoMasAlto.valor) {
          vecinoMasAlto = vecino;
        }
      }
    }
    return vecinoMasAlto;
  }

  private int obtenerHijos() {
    hijos.add(new Nodo(estado - 1, profundidad + 1));
    hijos.add(new Nodo(estado + 1, profundidad + 1));
    return 2;
  }

  Nodo(int estado, int profundidad) {
    this.estado = estado;
    this.profundidad = profundidad;
    valor = funcionEvaluacion(estado);
  }
}