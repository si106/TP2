package SI_TP2;

public class BusquedaLocal {
  public static void main(String[] args) {
    Problema problema = new Problema();
    int maximoLocal = HillClimbing(problema, false);
    System.out.println(maximoLocal);
  }

  public static int HillClimbing(Problema problema, boolean maximizar) {
    Nodo actual = problema.estadoInicial;
    Nodo vecinoMasAlto;
    do {
      if (maximizar) {
        vecinoMasAlto = actual.obtenerVecino(true);
        if (vecinoMasAlto.valor <= actual.valor) {
          return actual.estado;
        }

      } else {
        vecinoMasAlto = actual.obtenerVecino(false);
        if (vecinoMasAlto.valor >= actual.valor) {
          return actual.estado;
        }
      }
      actual = vecinoMasAlto;
    } while (true);
  }
}